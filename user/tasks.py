import logging
from django.template import Context
from django.template.loader import get_template
import os
from django.urls import reverse
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from ._celery import app
import message 
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.shortcuts import redirect
from celery.task import periodic_task
from celery.schedules import crontab  
from datetime import timedelta

@app.task
def send_verification_email(user_id):
    UserModel = get_user_model()
    try:
        user = UserModel.objects.get(pk=user_id)
        ctx = Context()
        ctx = {
            'user': user,         
              }
        text_content = render_to_string('text.txt',ctx).encode("utf-8")      
        html_content = render_to_string('email.html', ctx).encode("utf-8")
        subject = message.email_subject
        msg = EmailMultiAlternatives(subject, text_content, os.environ["EMAIL_GMAIL"],
         [user.email])
        msg.attach_alternative(html_content, "text/html")   
        msg.send()
    except UserModel.DoesNotExist:
        logging.warning(message.no_user_email % user_id)

