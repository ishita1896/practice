from django.contrib import admin
from django.conf.urls import url,include
from user import views
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^verify/(?P<uuid>[a-z0-9\-]+)/',views.VerifyView.as_view(), name='verify_user'),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^post/',include('publish.urls')),
    url(r'^item/',include('item.urls')),
    url(r'^chat/',include('chat.urls')),
    url(r'^pay/',include('order.urls')),
    url(r'^api/register/$',views.RegistrationAPIView.as_view(),name='register_api'),
    url(r'^api/users/$',views.ListUserAPIView.as_view(),name='user_api'),
    
]   

#  urls picked up from subapps 