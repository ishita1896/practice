from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from model_mommy import mommy
from user.serializers import UserListSerializer, UserCreateSerializer
from .models import User

class APITests(TestCase):
    
    def test_list_user(self):
        user = mommy.make(User)
        self.assertTrue(isinstance(user,User))
        user_serializer = UserListSerializer(user)
        assert user.email == user_serializer.data.get('email')
        assert user.full_name == user_serializer.data.get('full_name')
        assert user.password == user_serializer.data.get('password')

    def test_register(self):

        request_data = {
            "email" : "test@gmail.com",
            "password" : "test",
        }
        user_serializer = UserCreateSerializer(data=request_data)
        if user_serializer.is_valid():
            pass
        else:
            print("No found")

        user = User(email=request_data.get('email'), 
                    password=request_data.get('password'))

        assert request_data.get('email') == user_serializer.data.get('email')
        assert request_data.get('password') == user_serializer.data.get('password')



# class APITests(TestCase):

#     def setUp(self):
#         self.email = "test@gmail.com"
#         self.full_name = "test"
#         self.password = "123@"
#         self.is_staff = True
#         self.is_active = True
#         self.URLS = {
#                     'register-api': reverse('register_api'),
#                     'user-list-api':reverse('user_api')
#                     }

#     def register_user(self):
#         User.objects.create_user(email=self.email,
#                                  password=self.password,
#                                  full_name=self.full_name,
#                                  is_staff = self.is_staff,
#                                  is_active = self.is_active
#                                  )

#     def test_register(self):
#         client = APIClient()        
#         resp = client.post(self.URLS['register-api'],
#                            {'email': self.email, 'password': self.password,
#                             'full_name': self.full_name, 'is_staff' : self.is_staff,
#                             'is_active':self.is_active
#                             }, format='json')
#         assert resp.status_code == status.HTTP_200_OK    

#     def test_list_user(self):
#         client = APIClient()
#         resp = client.get(self.URLS['user-list-api'])
#         assert resp.status_code == status.HTTP_200_OK        