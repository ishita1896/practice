import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'user.settings')
 
app = Celery('user')
app.config_from_object('django.conf:settings')
 
# Load task modules from all registered Django app configs.
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend',
)

app.conf.CELERY_TIMEZONE = 'Asia/Kolkata' 
app.conf.CELERYBEAT_SCHEDULE =  {
    
    'send_verification_email': {
        'task': 'user.tasks.send_verification_email',
        'schedule': crontab(),
        'args':(['152',]),
    },

 
}

