from rest_framework import serializers
from django.db import transaction
from .models import User


class UserListSerializer(serializers.ModelSerializer):

	class Meta:
		model = User
		fields = '__all__'

class UserCreateSerializer(serializers.ModelSerializer):
	
 	def validate(self, data , *args, **kwargs):
 		return super(UserCreateSerializer, self).validate(data, *args, **kwargs)

	@transaction.atomic()
	def create(self,validated_data):
		user = User.objects.create(**validated_data)
		user.save()
		return user

	class Meta:
		model = User
		fields = ('email','password')


		

