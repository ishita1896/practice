no_email = "Email address must be provided"
no_pass = "Password must be provided"
no_user_email = "Tried to send verification email to non-existing user '%s'"
no_user_found = "User Does Not Exist"
not_found = "Not found"

no_item = "Item Does not exist"

chat_client_error = "ROOM_ACCESS_DENIED"
login_required = "USER_HAS_TO_LOGIN"
no_chat_room = "ROOM_INVALID"
email_subject = "Thanks!!"