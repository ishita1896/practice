from django.core.mail import send_mail
from django.core.mail import EmailMessage
import smtplib
from email.mime.text import MIMEText
import os

import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))

channel = connection.channel()

channel.queue_declare(queue='rpc_queue')


def mail(n):
    msg = "hello this mail by queue"
    server = smtplib.SMTP(os.environ["EMAIL_HOST"], 587)
    server.set_debuglevel(1)
    server.ehlo()
    server.starttls()
    server.login(os.environ["EMAIL_GMAIL"], os.environ["EMAIL_PASSWORD"])
    server.sendmail(os.environ["EMAIL_GMAIL"], n , msg)
    server.quit()
   

def on_request(ch, method, props, body):
    n = body
    print(" Mail to(%d)" , n)
    response = mail(n)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id = props.correlation_id),
                     body=n)
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='rpc_queue')

print(" [x] Awaiting RPC requests")
channel.start_consuming()