from django.http import Http404
from django.shortcuts import render, redirect
from .models import User
from django.views import View
from rest_framework.views import APIView
from .serializers import UserCreateSerializer, UserListSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import GenericAPIView


class VerifyView(View):

    def get(self,request,*args,**kwargs):
        try:
            user = User.objects.get(verification_uuid=self.kwargs["uuid"])
            user.is_verified = True
            user.save()
            return render(request,'home.html')
        except User.DoesNotExist:
            raise Http404(message.no_user_found)    


class RegistrationAPIView(APIView):
    serializer_class = UserCreateSerializer

    def post(self, request, *args, **kwargs):
        try:
            user_serializer = UserCreateSerializer(data=request.data)
            if user_serializer.is_valid():
                user = user_serializer.save()
                return Response(user_serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({'status': False,
                                 'message': "NOT REGISTERED"},
                                status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response({'status': False,
                             'message': str(e)},
                            status=status.HTTP_400_BAD_REQUEST)


class ListUserAPIView(GenericAPIView):
    serializer_class = UserListSerializer
    permission_classes = ()
    
    def get(self, request, format=None):
        try:
            users = User.objects.all()
            user_serializer = UserListSerializer(users, many=True)
            users = user_serializer.data
            return Response({'status': True,
                             'Response': users},
                            status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'status': False, 'message': str(e)},
                            status=status.HTTP_400_BAD_REQUEST)
