import datetime
from haystack import indexes
from .models import ItemModel

from .models import ItemModel
import django_filters

class ItemIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    title = indexes.CharField()
    description = indexes.CharField()

    def get_model(self):
        return ItemModel

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


class ItemFilter(django_filters.FilterSet):
    class Meta:
        model = ItemModel
        fields = '__all__'
