from haystack.forms import SearchForm


class ItemSearchForm(SearchForm):

    def search(self):
        # First, store the SearchQuerySet received from other processing.
        sqs = super(ItemSearchForm, self).search()

        if not self.is_valid():
            return self.no_query_found()

        # Check to see if a start_date was chosen.
        if self.cleaned_data['q']:
            sqs = sqs.filter(pub_date__gte=self.cleaned_data['q'])

   