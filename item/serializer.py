from drf_haystack.serializers import HaystackSerializer
from drf_haystack.viewsets import HaystackViewSet
from .models import ItemModel
from .search_indexes import ItemIndex

class ItemSerializer(HaystackSerializer):
  
     class Meta:
        
        index_classes = [ItemIndex]

        fields = [
            "title", "description"
        ]
   
class ItemSearchView(HaystackViewSet):
    index_models = [ItemModel]
    serializer_class = ItemSerializer
