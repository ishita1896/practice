from __future__ import unicode_literals
from django.shortcuts import render
from .models import ItemModel
from django.shortcuts import render_to_response
from .search_indexes import ItemFilter
from django.views import generic
from django.shortcuts import get_object_or_404

from .forms import ItemSearchForm

class ItemListView(generic.ListView):
	model = ItemModel
	context_object_name = 'item'
	template_name = 'items_all.html'


class ItemDetail(generic.DetailView):
    model = ItemModel
    context_object_name = 'item'
    template_name = 'detail.html'	
   

# def search(request,*args,**kwargs):
#     item_list = ItemModel.objects.all()
#     item_filter = ItemFilter(request.GET, queryset=item_list)
#     return render(request, 'item_list.html', {'filter': item_filter})
