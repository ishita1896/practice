import views
from rest_framework import routers
from .serializer import ItemSearchView 
from django.conf.urls import url,include

router = routers.DefaultRouter()
router.register("item/search", ItemSearchView,base_name='item_hay')

urlpatterns = [
	
	url(r'^all/', views.ItemListView.as_view(), name = 'all_items'),
	url(r'^detail/(?P<pk>\d+)/$',views.ItemDetail.as_view(), name= 'detail'),
	# url(r'^search/$',views.search, name= 'search'),
	url(r'^api/v1/',include(router.urls)),

]
