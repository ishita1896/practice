# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from .models import ItemModel
 
 
@admin.register(ItemModel)
class ItemAdmin(admin.ModelAdmin):
    pass