from django.conf.urls import url
import views
from django.contrib.auth.views import login, logout

urlpatterns = [
    url(r'^$', views.RoomListView.as_view(), name='homepage'),  # The start point for index view
   	url(r'^accounts/login/$', login, name='login'),  # The base django login view
    url(r'^accounts/logout/$', logout, name='logout'),  # The base django logout view
]	