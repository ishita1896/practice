from functools import wraps

from .exceptions import ClientError
from .models import Room
from user.message import login_required, chat_client_error, no_chat_room

def catch_client_error(func):
    @wraps(func)
    def inner(message, *args, **kwargs):
        try:
            return func(message, *args, **kwargs)
        except ClientError as e:
            e.send_to(message.reply_channel)
    return inner


def get_room_or_error(room_id, user):
    
    if not user.is_authenticated():
        raise ClientError(login_required)
    try:
        room = Room.objects.get(pk=room_id)
    except Room.DoesNotExist:
        raise ClientError(no_chat_room)
    if room.staff_only and not user.is_staff:
        raise ClientError(chat_client_error)
    return room

    