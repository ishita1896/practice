from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Room
from django.views import generic

class RoomListView(generic.ListView):
	model = Room
	context_object_name = 'rooms'
	template_name = 'index.html'