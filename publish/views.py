from django.http import Http404
from django.shortcuts import render
from .models import Post
from django.views import generic
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import PostListSerializer
from rest_framework.generics import GenericAPIView
from rest_framework import viewsets

class PostListView(generic.ListView):
	model = Post
	context_object_name = 'post'
	template_name = 'post_all.html'


class PostDetailView(generic.DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'post.html'    


class PostApiListView(GenericAPIView):

	serializer_class = PostListSerializer

	def get(self,request, *args, **kwargs):
		post = Post.objects.all()
		serializer = self.serializer_class(post, many = True).data
		return Response(serializer, status=status.HTTP_200_OK)
	
