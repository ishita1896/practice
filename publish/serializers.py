from rest_framework import serializers
from django.db import transaction
from .models import Post

class PostListSerializer(serializers.ModelSerializer):

	class Meta:
		model = Post
		fields = '__all__'