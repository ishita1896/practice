from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from .search import PostIndex
from django.dispatch import receiver

 
class Post(models.Model):
    author = models.ForeignKey(get_user_model())
    created = models.DateTimeField('Created Date', default=timezone.now)
    title = models.CharField('Title', max_length=200)
    content = models.TextField('Content')
    slug = models.SlugField('Slug')
 
    def __str__(self):
        return '"%s" by %s' % (self.title, self.author)

    @classmethod
    def indexing(self):
       obj = PostIndex(
          meta={'id': self.id},
          author=self.author.email,
          created=self.created,
          title=self.title,
          content=self.content,
          slug = self.slug
       )
       obj.save(index='post-index')
       print(obj.to_dict( ))
       return obj.to_dict(include_meta=True)     
         

