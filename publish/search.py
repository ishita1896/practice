from elasticsearch_dsl.connections import connections
from elasticsearch.helpers import streaming_bulk
from elasticsearch import Elasticsearch
from elasticsearch_dsl import DocType, Text, Date, Search  
connections.create_connection()

class PostIndex(DocType):
    author = Text()
    created = Date()
    title = Text()
    content = Text()

    class Meta:
        index = 'post-index'

def bulk_indexing():
    PostIndex.init(index='post-index')
    from .models import Post
    es = Elasticsearch()
    s=streaming_bulk(client=es, actions=(post.indexing for post in Post.objects.all())
        )        

def search(author):

    client = Elasticsearch()
    s = Search(using=client,index='post-index').filter('term',author=author)
    response = s.execute()
    return response

