from django.apps import AppConfig
# import algoliasearch_django as algoliasearch


class PublishConfig(AppConfig):
    name = 'publish'

    def ready(self):
        import publish.signals
