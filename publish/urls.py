from django.conf.urls import url,include
from . import views

urlpatterns = [
	url(r'^all/$',views.PostListView.as_view(),name = "all_post"),
	url(r'^detail/(?P<pk>\d+)/$',views.PostDetailView.as_view(), name = "post_detail"),
	url(r'^api/list/$',views.PostApiListView.as_view(), name="post_list")
		]