from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status

class APITests(TestCase):

	def setUp(self):
		self.title = "test"
		self.content = "hello"
		self.author = "testauth"
		self.slug = "test"
		self.URLS = {
					'list_url': reverse('post_list')
					}

	def test_list_post(self):
		client = APIClient()
		resp = client.get(self.URLS['list_url'])
		assert resp.status_code == status.HTTP_200_OK				
		