from django.conf.urls import url,include
from . import views
from django.views.generic import TemplateView
from rest_framework import routers

urlpatterns = [
	url(r'^order/pay/(?P<pk>\d+)/$',views.OrderListView.as_view(),name='order_pay'),
  	url(r'^payment_process/', views.PayPaypalView.as_view(), name='payment_process' ),
	url(r'^payment_done/', TemplateView.as_view(template_name= "paypal/payment_done.html"), name='payment_done'),
	url(r'^payment_canceled/$', TemplateView.as_view(template_name= "paypal/payment_canceled.html"), name='payment_canceled'),
	#stripe payment
	url(r'^payment_form/(?P<pk>\d+)/$',views.PaystripeView.as_view(), name='stripe_payment'),
	url(r'^checkout/(?P<pk>\d+)/$', views.CheckoutView.as_view(), name="checkout_page"),
	]

