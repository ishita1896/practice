from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response ,render, get_object_or_404
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from django.shortcuts import render
from django.http import HttpResponse , Http404
from user import settings
from publish.models import Post
import datetime
import stripe
from django.views import generic
from django.views import View
from django.utils.decorators import method_decorator
from user import message
from django.shortcuts import redirect
from item.models import ItemModel
stripe.api_key = settings.STRIPE_SECRET_KEY


class OrderListView(generic.DetailView):
    model = ItemModel
    context_object_name = 'item'
    template_name = 'order_button.html'


class PayPaypalView(View):
    def get(self,request):
      try:
        host = request.get_host()
        paypal_dict = {
            'business': settings.PAYPAL_RECEIVER_EMAIL ,
            'amount': '2000',
            'item_name': 'Test',
            'invoice': 'Test Payment Invoice',
            'currency_code': 'USD',
            'notify_url': 'http://{}{}'.format(host, reverse('paypal-ipn')),
            'return_url': 'http://{}{}'.format(host, reverse('payment_done')),
            'cancel_return': 'http://{}{}'.format(host, reverse('payment_canceled')),
            }   
        form = PayPalPaymentsForm(initial=paypal_dict)
        return render(request, 'paypal/payment_process.html', {'form': form })
      except Exception as e:
        raise Http404(message.not_found)  


class PaystripeView(generic.DetailView):
    model = ItemModel
    template_name = 'stripe/final_payment.html' 

    def get_context_data(self, **kwargs):
        context = super(PaystripeView, self).get_context_data(**kwargs)
        context['item'] = ItemModel.objects.get(id=self.kwargs['pk'])
        context['stripe_key'] = settings.STRIPE_PUBLIC_KEY
        return context


class CheckoutView(View):

    def post(self,request,*args,**kwargs):
        try:
          item = ItemModel.objects.get(id=self.kwargs["pk"]) 
          if request.method == "POST":
              token    = request.POST.get("stripeToken")
          try:
              charge  = stripe.Charge.create(
                  amount      = int(item.price),
                  currency    = "usd",
                  source      = request.POST.get("stripeToken"),
                  description = "The product charged to the user"
              )
              
          except stripe.error.CardError as ce:
              return False, ce
          else:
              charge.save()
              return render(request,'repay.html')

        except ItemModel.DoesNotExist :
            raise Http404(message.no_item)      